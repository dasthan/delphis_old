import json, falcon
import pyodbc
import math
from money import Money
import babel
import re

cnxn = pyodbc.connect('DSN=DelphisERP;UID=SA;PWD=sql2012@')
cursor = cnxn.cursor()

class OpenOffersClass:
	def on_post(self, req, resp):
		offers = []
		my_offers = 0
		
		cursor.execute("SELECT A.SODTYPE,A.PRJC,B.SODTYPE AS X_05CC8500,B.PRJC AS X_05CC85D0,B.UTBL01 AS X_05CC7DB0,B.UTBL02 AS X_05CC7CE0,B.UTBL03 AS X_05CC7C10,A.CODE,A.NAME,A.ISACTIVE,A.PRJCRM,A.PRJSTATE,A.BUSUNITS,A.TRDR,C.NAME AS X_TNAME,C.ISPROSP AS X_TISPROSP,C.TRDPGROUP AS X_05A810C0,A.SALESMAN,D.NAME2 AS X_NAME2,A.PRJCLEAD,ISNULL(A.INVAL,0) AS INVAL,ISNULL(A.ISVAL,0) AS ISVAL,A.CRDDATE,A.FROMDATE,A.CLOSEDATE,ISNULL(((A.INVAL * (A.ISVAL/100))),0) AS WeightedRev FROM ((PRJC A LEFT OUTER JOIN PRJEXTRA B ON A.PRJC=B.PRJC) LEFT OUTER JOIN TRDR C ON A.TRDR=C.TRDR) LEFT OUTER JOIN PRSN D ON A.SALESMAN=D.PRSN WHERE A.COMPANY=2 AND A.SODTYPE=40 AND A.PRJCRM=2 ORDER BY A.CODE,A.PRJC")
		for row in cursor.fetchall():
			offers.append(list(row))

		for each in offers[0:len(offers)]:	
			state = each[11]
			amount = each[20]
			date = each[22].strftime("%b %d, %Y")
			customer = each[14]
			if state == 1003:
				my_offers = my_offers + amount
		
		f = Money(amount=str(my_offers), currency="EUR")		
		m = f.format('es_ES')
		content = {
			"text": "You have" + " " + "*"+m+"*" + " " + "in open offers"
		}
		resp.body = json.dumps(content)


class AmountContractsClass:
	def on_post(self, req, resp):
		contracts = []
		amount_contracts = 0
		
		cursor.execute("SELECT A.SODTYPE,A.CNTR,B.COMPANY AS X_19556160,B.CNTRLINES AS X_19555FC0,B.VISITUNIT AS X_16D6DD60,B.VISITNUM AS X_16D6DE30,ISNULL(B.PRICE,0) AS X_16D6E310,B.ISACTIVE AS X_1963B470,B.CCCFREQUENCY AS X_1963B540,A.CODE,A.ISACTIVE,A.TRNDATE,A.FROMDATE,A.TODATE,A.TRDR,D.COMPANY AS X_1A4F1570,D.SODTYPE AS X_1A4F07A0,D.TRDR AS X_1A4F0940,D.CODE AS X_1A4F0A10,D.NAME AS X_1A4F0BB0,D.ISPROSP AS X_1A4F25B0,D.SOCURRENCY AS X_1A4F2000,(select inst.name from inst where inst.inst=b.inst) AS V1 FROM ((CNTR A  LEFT OUTER  JOIN CNTRLINES B ON B.CNTR=A.CNTR) LEFT OUTER JOIN TRDR C ON A.TRDR=C.TRDR) LEFT OUTER JOIN TRDR D ON A.TRDR=D.TRDR WHERE A.COMPANY=2 AND A.SODTYPE=42 AND A.ISACTIVE=1 AND B.ISACTIVE=1 ORDER BY A.CODE,D.NAME,A.CNTR")
		for row in cursor.fetchall():
			contracts.append(list(row))

		for each in contracts[0:len(contracts)]:
			amount = float(each[6])
			amount_contracts = amount_contracts + amount
			f = Money(amount=str(amount_contracts), currency="EUR")
			m = f.format('es_ES')
		
		f = Money(amount=str(amount_contracts), currency="EUR")		
		m = f.format('es_ES')
		content = {
			"text": "You have" + " " + "*"+m+"*" + " " + "in yearly active contracts"
		}
		resp.body = json.dumps(content)


class InstallationsUnderContract:
	def on_post(self, req, resp):
		installations = []
		
		cursor.execute("SELECT A.SODTYPE,A.CNTR,B.COMPANY AS X_1C734040,B.CNTRLINES AS X_1C734520,B.VISITUNIT AS X_1C734AD0,B.VISITNUM AS X_1C734BA0,ISNULL(B.PRICE,0) AS X_1C735080,B.ISACTIVE AS X_1C1796B0,B.CCCFREQUENCY AS X_1C179780,A.CODE,A.ISACTIVE,A.TRNDATE,A.FROMDATE,A.TODATE,A.TRDR,D.COMPANY AS X_1C732980,D.SODTYPE AS X_1C732B20,D.TRDR AS X_1C732BF0,D.CODE AS X_1C732CC0,D.NAME AS X_1C732D90,D.ISPROSP AS X_1C72CDB0,D.SOCURRENCY AS X_1C72D1C0,(select inst.name from inst where inst.inst=b.inst) AS V1 FROM ((CNTR A  LEFT OUTER  JOIN CNTRLINES B ON B.CNTR=A.CNTR) LEFT OUTER JOIN TRDR C ON A.TRDR=C.TRDR) LEFT OUTER JOIN TRDR D ON A.TRDR=D.TRDR WHERE A.COMPANY=2 AND A.SODTYPE=42 AND A.ISACTIVE=1 AND B.ISACTIVE=1 ORDER BY A.CODE,D.NAME,A.CNTR")
		for row in cursor.fetchall():
			installations.append(list(row))

		count_installations = str(len(installations))

		content = {
			"text": "You have" + " " + count_installations + " " + "installations under contract"
		}
		resp.body = json.dumps(content)

class BuildingsUnderContract:
	def on_post(self, req, resp):
		installations = []
		
		cursor.execute("SELECT A.SODTYPE,A.CNTR,B.COMPANY AS X_19ED4D80,B.CNTRLINES AS X_19ED4B10,B.VISITUNIT AS X_19ED9EC0,B.VISITNUM AS X_19ED9DF0,ISNULL(B.PRICE,0) AS X_19ED9C50,B.ISACTIVE AS X_19ED9770,B.CCCFREQUENCY AS X_19ED9910,A.CODE,A.ISACTIVE,A.TRNDATE,A.FROMDATE,A.TODATE,A.TRDR,D.COMPANY AS X_1B9CC330,D.SODTYPE AS X_1B9C98F0,D.TRDR AS X_1B9C9B60,D.CODE AS X_1B9C9C30,D.NAME AS X_1B9C9D00,D.ISPROSP AS X_1B9C94E0,D.SOCURRENCY AS X_1B9C9680,A.TRDBRANCH,E.NAME AS X_19F25C50,A.BRANCH,(select inst.name from inst where inst.inst=b.inst) AS V1,(select inst.gpnt from inst where inst.inst=b.inst) AS V2 FROM (((CNTR A  LEFT OUTER  JOIN CNTRLINES B ON B.CNTR=A.CNTR) LEFT OUTER JOIN TRDR C ON A.TRDR=C.TRDR) LEFT OUTER JOIN TRDR D ON A.TRDR=D.TRDR) LEFT OUTER JOIN TRDBRANCH E ON A.TRDBRANCH=E.TRDBRANCH WHERE A.COMPANY=2 AND A.SODTYPE=42 AND A.ISACTIVE=1 AND B.ISACTIVE=1 ORDER BY A.CODE,D.NAME,A.CNTR")
		for row in cursor.fetchall():
			installations.append(list(row))

		gpnt_list = []
		for each in installations:
			gpnt = each[len(each)-1]
			gpnt_list.append(gpnt)
		

		unique_gpnt = set(gpnt_list)
		count_buildings = str(len(unique_gpnt))

		content = {
			"text": "You have" + " " + count_buildings + " " + "buildings under contract"
		}
		resp.body = json.dumps(content)



api = falcon.API()
api.add_route('/open_offers', OpenOffersClass())
api.add_route('/amount_contracts', AmountContractsClass())
api.add_route('/installations_under_contract', InstallationsUnderContract())
api.add_route('/buildings_under_contract', BuildingsUnderContract())






