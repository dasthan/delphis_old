#!/usr/bin/env python
# -*- coding: utf-8 -*- 

from __future__ import unicode_literals
import dash
from   dash.dependencies import Input, Output
import dash_core_components as dcc
import dash_html_components as html
import dash_table as dt
import pandas as pd

import pyodbc
import csv

import pyodbc
import pandas as pd




# Connection to ERP

cnxn = pyodbc.connect('DSN=DelphisERP;UID=SA;PWD=sql2012@')
cursor = cnxn.cursor()

alldata = []

cursor.execute("SELECT A.acnt,B.CODE AS X_0AE46B50,B.NAME AS X_0AE46DC0,A.TRNDATE,A.FINCODE,A.CODE,A.NAME,ISNULL(A.AXIA,0) AS AXIA,A.COMMENTS FROM CCCANALSTAT A LEFT OUTER JOIN ACNT B ON A.acnt=B.ACNT WHERE A.TRNDATE>='20190101' AND A.TRNDATE<'20190331' ORDER BY A.acnt")
for row in cursor.fetchall():
    alldata.append(list(row))

alldata2 = []    
    
cursor.execute("SELECT A.COMPANY,A.FINDOC,B.FINDOC AS X_MFINDOC,C.COMPANY AS X_156F7020,C.MTRLINES AS X_156F71C0,C.SODTYPE AS X_156F7360,C.MTRL AS X_156F7430,D.COMPANY AS X_1570BB70,D.SODTYPE AS X_1570BAA0,D.MTRL AS X_1570C870,ISNULL((SELECT SUM(PURFIFO) FROM MTRCPRICES WHERE MTRL=D.MTRL AND COMPANY=2 AND FISCPRD=2019 AND PERIOD=1000),0) AS X_1574CAC0,ISNULL((SELECT SUM(PURFIFO) FROM MTRCPRICES WHERE MTRL=D.MTRL AND COMPANY=2 AND FISCPRD=2018 AND PERIOD=1000),0) AS X_1574CB90,ISNULL((SELECT SUM(PURFIFO) FROM MTRCPRICES WHERE MTRL=D.MTRL AND COMPANY=2 AND FISCPRD=2017 AND PERIOD=1000),0) AS X_1574CC60,ISNULL((SELECT SUM(PURFIFO) FROM MTRCPRICES WHERE MTRL=D.MTRL AND COMPANY=2 AND FISCPRD=2016 AND PERIOD=1000),0) AS X_1574CD30,D.CODE AS X_1570BC40,D.NAME AS X_1570BD10,D.MTRTYPE AS X_1570C2C0,D.MTRTYPE1 AS X_1570C390,C.SOSOURCE AS X_156F76A0,C.SOREDIR AS X_156F7770,ISNULL(C.QTY1,0) AS X_156F8FD0,A.SOSOURCE,A.SOREDIR,A.TRNDATE,A.SERIES,A.FINCODE,A.SODTYPE,A.TRDR,F.CODE AS X_CODE,F.NAME AS X_TNAME,F.ISPROSP AS X_ISPROSP,F.SOCURRENCY AS X_SOCURRENCY,F.CMPMODE AS X_CMPMODE,A.TRDBRANCH,I.TRDBRANCH AS X_15748780,I.TRDR AS X_15748850,I.SODTYPE AS X_15748920,I.NAME AS X_15748B90,I.BRANCH AS X_1574A3F0,A.VATSTS,A.ISPRINT,A.APPRV,A.BUSUNITS,ISNULL(A.NETAMNT,0) AS NETAMNT FROM ((((((FINDOC A LEFT OUTER JOIN MTRDOC B ON A.FINDOC=B.FINDOC)  LEFT OUTER  JOIN MTRLINES C ON C.FINDOC=A.FINDOC) LEFT OUTER JOIN MTRL D ON C.MTRL=D.MTRL) LEFT OUTER JOIN TRDR F ON A.TRDR=F.TRDR) LEFT OUTER JOIN TRDEXTRA G ON A.TRDR=G.TRDR) LEFT OUTER JOIN TRDBRANCH H ON A.TRDBRANCH=H.TRDBRANCH) LEFT OUTER JOIN TRDBRANCH I ON A.TRDR=I.TRDR AND A.TRDBRANCH=I.TRDBRANCH WHERE A.COMPANY=2 AND A.SOSOURCE=1351 AND A.SOREDIR=0 AND A.TRNDATE>='20190101' AND A.TRNDATE<'20190331' AND A.SERIES IN (7400,7401,7402,7403,7404,7405,7406,7407,7408,7500,8400,8401,8402,8408,8500,7606,8600,27400,27500,27606,28500) AND A.SODTYPE=13 AND C.SODTYPE IN (51) ORDER BY A.TRNDATE DESC,A.FINDOC")
for row in cursor.fetchall():
    alldata2.append(list(row))  

alldata3 = [] 

cursor.execute("SELECT A.Company,A.TrnDate,A.Findoc,A.SoSource,A.SoReDir,A.Fprms,A.Series,A.FinCode,A.TSodType,A.Trdr,B.NAME AS X_TNAME,B.CMPMODE AS X_TCMPMODE,A.BusUnits,A.MSodType,A.Mtprns,A.Mtrl,C.CODE AS X_MCODE,C.NAME AS X_MNAME,C.MTRGROUP AS X_MTRGROUP,A.MtrTrn,ISNULL(A.SalQty1,0) AS SalQty1,ISNULL(A.SalVal,0) AS SalVal,ISNULL(A.SCstandcost,0) AS SCstandcost,ISNULL(A.SCpurmtk,0) AS SCpurmtk,ISNULL(A.SCpurmmtk,0) AS SCpurmmtk,ISNULL(A.SCpurtmt,0) AS SCpurtmt,ISNULL(A.SCpurlprice,0) AS SCpurlprice,ISNULL(A.SCpurfifo,0) AS SCpurfifo,ISNULL(A.SCpurlifo,0) AS SCpurlifo,ISNULL(A.SCpurprice1,0) AS SCpurprice1 FROM (VMTRSTAT A LEFT OUTER JOIN TRDR B ON A.Trdr=B.TRDR) LEFT OUTER JOIN MTRL C ON A.Mtrl=C.MTRL WHERE A.COMPANY=2 AND A.TrnDate>='20190101' AND A.TrnDate<'20190331' AND A.SoSource=1351 AND A.Series IN (7100,7101,7102,7103,7104,7105,7106,7200,7201,7202,7300,7301,7302,7303,7600,7601,7602,7603,7604,7605,7606,8100,8101,8102,8200,8201,8202,8300,8301,8302,8600,8601,27100) AND A.TSodType=13 ORDER BY A.Findoc,A.MtrTrn")
for row in cursor.fetchall():
    alldata3.append(list(row))      

# SPLIT DATA PER BUSINESS UNIT

def perbu(mycompletelist, pl_bu):
    service = []
    hvac = []
    refrigeration = []
    general_allocation = []
    for each in mycompletelist:       
        code = each[1]
        bu = each[8]
        amount = each[7]
        date = each[3]
        if  (bu == "SERVICE - ΣΥΜΒΟΛΑΙΑ - HVAC"
            or   bu == "SERVICE - ΣΥΜΒΟΛΑΙΑ - REFRIG."
            or   bu == "SERVICE - ΣΥΜΒΟΛΑΙΑ - TOTALS"
            or   bu == "SERVICE - ΣΥΜΒΟΛΑΙΑ - ONLINE"
            or   bu == "SERVICE - ΕΚΤΑΚΤΑ - HVAC"
            or   bu == "SERVICE - ΕΚΤΑΚΤΑ - REFRIG."
            or   bu == "SERVICE - ΕΚΤΑΚΤΑ - TOTALS"
            or   bu == "SERVICE - ΕΚΤΑΚΤΑ - ONLINE"
            or   bu == "SERVICE - EXTRAS - AFTER SALES"
            or   bu == "SERVICE - ALLOCATE - ITEMS"
            or   bu == "DELPHIS SMART BUILDING"
            or   bu == "SERVICE - ΣΥΜΒΟΛΑΙΑ - ΕΛΠΕ"
            or   bu == "THESSALONIKI - SERVICE - ΣΥΜΒΟΛΑΙΑ - HVAC"
            or   bu == "THESSALONIKI - SERVICE - ΣΥΜΒΟΛΑΙΑ - TOTAL"
            or   bu == "THESSALONIKI - SERVICE - EKTAKTA - HVAC"
            or   bu == "THESSALONIKI - SERVICE - EKTAKTA - TOTAL"
            or   bu == "THESSALONIKI - SERVICE - ΕΚΤΑΚΤΑ - REFRI"
            ):
                service.append([date, date.month, amount, code])
                
        if  (bu == "ΕΜΠΟΡΙΚΟ - COMMERCIAL - HVAC"
            or   bu == "ΕΜΠΟΡΙΚΟ - INSTALLATION - HVAC"
            or   bu == "THESSALONIKI - COMMERCIAL"
            ):
                hvac.append([date, date.month, amount, code])
                
        if  bu == "ΕΜΠΟΡΙΚΟ - COMMERCIAL - REFRIG.":
                refrigeration.append([date, date.month, amount, code])
                
        if (bu == "GENERAL - ALLOCATION - ITEMS"
            or bu == "GENERAL - ALLOCATION – THD "
            or bu == "GENERAL - ALLOCATION – DLD "
           ):
                general_allocation.append([date, date.month, amount, code])
                    
    if pl_bu == "service":
        return service
    if pl_bu == "hvac":
        return hvac
    if pl_bu == "refrigeration":
        return refrigeration
    if pl_bu == "general_allocation":
        return general_allocation
        
    
service_data = perbu(alldata, "service")
hvac_data = perbu(alldata, "hvac")
refrigeration_data = perbu(alldata, "refrigeration")
general_allocation_data = perbu(alldata, "general_allocation")

# print(service_data)
# print(hvac_data)
# print(refrigeration_data)




# Create first p&l from tags


df_service_data = pd.DataFrame.from_records(service_data)
df_hvac_data = pd.DataFrame.from_records(hvac_data)
df_refrigeration_data = pd.DataFrame.from_records(refrigeration_data)

def addtags(bu_data):
    f = open("data/data_mapping.csv")
    read = csv.reader(f)
    data_mapping = list(read)

    df_mapping = pd.DataFrame.from_records(data_mapping)
    header = df_mapping.iloc[0]
    df_mapping2 = df_mapping[1:]
    map_table = df_mapping2.rename(columns = header)
    bu_data_final = bu_data.rename(columns={0: "Date", 1: "Month", 2: "Amount", 3: "Code"})

    mergedStuff = pd.merge(bu_data_final, map_table, on=['Code'], how='inner')
    
    a = mergedStuff["Category A"].astype('category')
    b = mergedStuff["Category B"].astype('category')
    c = mergedStuff["Category C"].astype('category')
    
    indexa = pd.CategoricalIndex(a, categories=["sales","total cogs","direct costs","bank_expense"])
    indexb = pd.CategoricalIndex(b, categories=["cogs","subcontractors","transportation","warranties","recycling","staff cost","car cost","marketing cost","other direct costs"])
    indexc = pd.CategoricalIndex(c, categories=["salaries","subcontractors staff","clothing","Education","insurance","fuel","repair & service cost","leasing","insurance car", "tax fees","parking & kteo & toll", "advertising","hospitality","exhibition","subscriptions","other marketing cost","mobile","rent","other direct costs"])  
    
    multi_data = mergedStuff.set_index(["Month",indexa, indexb, indexc], append=True).sort_index(level=2)

    data = multi_data[["Amount"]]
    
    final = data.unstack(level=1)
    
    final2 = final.set_index([indexa, indexb, indexc]).sort_index(level=2)
    
    dfsum0 = final2.groupby(level=0, as_index=True).sum()
    dfsum1 = final2.groupby(level=1, as_index=True).sum()
    dfsum2 = final2.groupby(level=2, as_index=True).sum()
   
    a = dfsum0.stack(level=0)
    b = dfsum1.stack(level=0)
    c = dfsum2.stack(level=0)
    
    aa = a.reset_index()
    bb = b.reset_index()
    cc = c.reset_index()
    
    final3 = pd.concat([aa,bb,cc],sort=True)
    
    a1 = final3["Category A"].astype('category')
    b1 = final3["Category B"].astype('category')
    c1 = final3["Category C"].astype('category')
    
    indexa1 = pd.CategoricalIndex(a1, categories=["sales","total cogs","direct costs","allocation cost","bank_expense","gross_profit","ebitda","ebtda"])
    indexb1 = pd.CategoricalIndex(b1, categories=["cogs","subcontractors","transportation","warranties","recycling","staff cost","car cost","marketing cost","other direct costs","total"])
    indexc1 = pd.CategoricalIndex(c1, categories=["salaries","subcontractors staff","clothing","Education","insurance","fuel","repair & service cost","leasing","insurance car", "tax fees","parking & kteo & toll", "advertising","hospitality","exhibition","subscriptions","other marketing cost","mobile","rent","other direct costs","total"])  
    
    new_multi_data = final3.set_index([indexa1, indexb1, indexc1]).sort_index(level=2)
    
    
    new = new_multi_data.index.set_codes([[1, 1, 1, 1, 1, 2, 2, 2, 2, 0, 1, 2, 4, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2], 
 [0, 1, 2, 3, 4, 5, 6, 7, 8,9, 9,9, 9,5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 8, 8, 8], 
 [19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19, 19,19, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18 ]])

    pnl = new_multi_data.set_index(new).sort_index(level=0)
    
    pnl_final = pnl.drop(['Category A', 'level_1','Category B','Category C'], axis=1)
    
    return pnl_final


pnl_service = addtags(df_service_data)
pnl_hvac = addtags(df_hvac_data)
pnl_refrigeration = addtags(df_refrigeration_data)



# Expand pnl with general allocation cost

df_general_allocation_data = pd.DataFrame.from_records(general_allocation_data)


# Prepare general allocation data

def merge_general_allocation(bu_data):
    f = open("data/general_allocation.csv")
    read = csv.reader(f)
    data_mapping = list(read)

    df_mapping = pd.DataFrame.from_records(data_mapping)
    header = df_mapping.iloc[0]
    df_mapping2 = df_mapping[1:]
    map_table = df_mapping2.rename(columns = header)
    bu_data_final = bu_data.rename(columns={0: "Date", 1: "Month", 2: "Amount", 3: "Code"})

    mergedStuff = pd.merge(bu_data_final, map_table, on=['Code'], how='inner')

    return  mergedStuff

merged_general_allocation = merge_general_allocation(df_general_allocation_data)


def general_allocation(merged_data):

    merged_data["Amount HVAC"] = round(merged_data["Amount"].astype(float)*merged_data["HVAC"].astype(float), 2)
    merged_data["Amount Service"] = round(merged_data["Amount"].astype(float)*merged_data["Service"].astype(float), 2)
    merged_data["Amount Refrigeration"] = round(merged_data["Amount"].astype(float)*merged_data["Refrigeration"].astype(float), 2)
    merged_data["Category A"] = "allocation cost"
     
    multi_data = merged_data.set_index(["Month","Category A"], append=True).sort_index(level=1)

    data = multi_data[["Amount HVAC","Amount Service","Amount Refrigeration"]]
    
    final = data.unstack(level=1) 
    x = final.reset_index()  
    f = x.set_index(["Category A"])
    d = f[["Amount HVAC","Amount Service","Amount Refrigeration"]]
    
    dfsum0 = d.groupby(level=0, as_index=True).sum()
    dfsum0["Category B"] = "NaN"
    dfsum0["Category C"] = "NaN"
    
    ou = dfsum0.reset_index()
    
    a2 = ou["Category A"].astype('category')
    b2 = ou["Category B"].astype('category')
    c2 = ou["Category C"].astype('category')
    
    indexa2 = pd.CategoricalIndex(a2, categories=["allocation cost"])
    indexb2 = pd.CategoricalIndex(b2, categories=["NaN"])
    indexc2 = pd.CategoricalIndex(c2, categories=["NaN"])
     
    multi_data2 = ou.set_index([indexa2, indexb2, indexc2]).sort_index(level=2)
    
    final = multi_data2[["Amount HVAC","Amount Service","Amount Refrigeration"]]
    
    return final
    
general_allocation_data = general_allocation(merged_general_allocation)

q = general_allocation_data["Amount HVAC"]
w = general_allocation_data["Amount Service"]
e = general_allocation_data["Amount Refrigeration"]

q.index = q.index.set_levels([['allocation cost','sales', 'total cogs', 'gross_profit','direct costs',"bank_expense","ebitda","ebtda"], ['total','cogs', 'subcontractors', 'transportation', 'warranties', 'recycling', 'staff cost', 'car cost', 'marketing cost', 'other direct costs'], ['total','salaries', 'subcontractors staff', 'clothing', 'Education', 'insurance', 'fuel', 'repair & service cost', 'leasing', 'insurance car', 'tax fees', 'parking & kteo & toll', 'advertising', 'hospitality', 'exhibition', 'subscriptions', 'other marketing cost', 'mobile', 'rent', 'other direct costs']])
w.index = w.index.set_levels([['allocation cost','sales', 'total cogs', 'gross_profit','direct costs',"bank_expense","ebitda","ebtda"], ['total','cogs', 'subcontractors', 'transportation', 'warranties', 'recycling', 'staff cost', 'car cost', 'marketing cost', 'other direct costs'], ['total','salaries', 'subcontractors staff', 'clothing', 'Education', 'insurance', 'fuel', 'repair & service cost', 'leasing', 'insurance car', 'tax fees', 'parking & kteo & toll', 'advertising', 'hospitality', 'exhibition', 'subscriptions', 'other marketing cost', 'mobile', 'rent', 'other direct costs']])
e.index = e.index.set_levels([['allocation cost','sales', 'total cogs', 'gross_profit','direct costs',"bank_expense","ebitda","ebtda"], ['total','cogs', 'subcontractors', 'transportation', 'warranties', 'recycling', 'staff cost', 'car cost', 'marketing cost', 'other direct costs'], ['total','salaries', 'subcontractors staff', 'clothing', 'Education', 'insurance', 'fuel', 'repair & service cost', 'leasing', 'insurance car', 'tax fees', 'parking & kteo & toll', 'advertising', 'hospitality', 'exhibition', 'subscriptions', 'other marketing cost', 'mobile', 'rent', 'other direct costs']])

# Concat tables
pnl_hvac_all = pd.concat([pnl_hvac, q])
pnl_service_all = pd.concat([pnl_service, w])
pnl_refrigeration_all = pd.concat([pnl_refrigeration, e])

# Bank expense

def addtagsbank(bu_data):
    f = open("data/data_mapping.csv")
    read = csv.reader(f)
    data_mapping = list(read)

    df_mapping = pd.DataFrame.from_records(data_mapping)
    header = df_mapping.iloc[0]
    df_mapping2 = df_mapping[1:]
    map_table = df_mapping2.rename(columns = header)
    bu_data_final = bu_data.rename(columns={0: "Date", 1: "Month", 2: "Amount", 3: "Code"})

    mergedStuff = pd.merge(bu_data_final, map_table, on=['Code'], how='inner')
    
    return mergedStuff

merged_service = addtagsbank(df_service_data)
merged_hvac = addtagsbank(df_hvac_data)
merged_refrigeration = addtagsbank(df_refrigeration_data)


def bank_expense(bu_merged):
    bank_expense_dict = {}
    data_bool = bu_merged["Category A"] == "bank_expense" 
    bank_expense = bu_merged.loc[data_bool, "Amount"].sum()
     
    bank_expense_dict["bank_expense"] = bank_expense
     
    return bank_expense_dict

bank_expense_service = bank_expense(merged_service)
bank_expense_hvac = bank_expense(merged_hvac)
bank_expense_refrigeration = bank_expense(merged_refrigeration)


def merge_bank_allocation(bu_data):
    f = open("data/bank_allocation.csv")
    read = csv.reader(f)
    data_mapping = list(read)

    df_mapping = pd.DataFrame.from_records(data_mapping)
    header = df_mapping.iloc[0]
    df_mapping2 = df_mapping[1:]
    map_table = df_mapping2.rename(columns = header)
    bu_data_final = bu_data.rename(columns={0: "Date", 1: "Month", 2: "Amount", 3: "Code"})

    mergedStuff = pd.merge(bu_data_final, map_table, on=['Code'], how='inner')

    return  mergedStuff

merged_bank_allocation = merge_bank_allocation(df_general_allocation_data)


def bank_allocation(merged_data):
  
    merged_data["Amount HVAC"] = merged_data["Amount"].astype(float)*merged_data["HVAC"].astype(float)
    merged_data["Amount Service"] = merged_data["Amount"].astype(float)*merged_data["Service"].astype(float)
    merged_data["Amount Refrigeration"] = merged_data["Amount"].astype(float)*merged_data["Refrigeration"].astype(float)
    merged_data["Category A"] = "bank expense"
    
    multi_data = merged_data.set_index(["Month","Category A"], append=True).sort_index(level=1)
    
    data = multi_data[["Amount HVAC","Amount Service","Amount Refrigeration"]]
    
    final = data.unstack(level=1) 
    x = final.reset_index()  
    f = x.set_index(["Category A"])
    d = f[["Amount HVAC","Amount Service","Amount Refrigeration"]]
    
    dfsum0 = d.groupby(level=0, as_index=True).sum()
    dfsum0["Category B"] = "NaN"
    dfsum0["Category C"] = "NaN"
    
    ou = dfsum0.reset_index()
    
    a2 = ou["Category A"].astype('category')
    b2 = ou["Category B"].astype('category')
    c2 = ou["Category C"].astype('category')
    
    indexa2 = pd.CategoricalIndex(a2, categories=["bank expense"])
    indexb2 = pd.CategoricalIndex(b2, categories=["NaN"])
    indexc2 = pd.CategoricalIndex(c2, categories=["NaN"])
     
    multi_data2 = ou.set_index([indexa2, indexb2, indexc2]).sort_index(level=2)
    
    final = multi_data2[["Amount HVAC","Amount Service","Amount Refrigeration"]]
    
    return final
    
    
bank_allocation_data = bank_allocation(merged_bank_allocation)


#addition of bank expense (mainly for checking purposes)

idx = pd.IndexSlice

#hvac bank
hvac_bank_a = bank_allocation_data.loc[idx["bank expense"],("Amount HVAC")]
hvac_bank_b = pnl_hvac_all.loc[idx["bank_expense"]]
total_hvac_1 = hvac_bank_a.values + hvac_bank_b.values
bank_allocation_data.loc[idx["bank expense"],("Amount HVAC")] = total_hvac_1
#service bank
service_bank_a = bank_allocation_data.loc[idx["bank expense"],("Amount Service")]
service_bank_b = pnl_service_all.loc[idx["bank_expense"]]
total_service_1 = service_bank_a.values + service_bank_b.values
bank_allocation_data.loc[idx["bank expense"],("Amount Service")] = total_service_1
#refrigeration bank
refrigeration_bank_a = bank_allocation_data.loc[idx["bank expense"],("Amount Refrigeration")]
refrigeration_bank_b = pnl_refrigeration_all.loc[idx["bank_expense"]]
total_refrigeration_1 = refrigeration_bank_a.values + refrigeration_bank_b.values
bank_allocation_data.loc[idx["bank expense"],("Amount Refrigeration")] = total_refrigeration_1



# p&l with bank data

q1 = bank_allocation_data["Amount HVAC"]
w1 = bank_allocation_data["Amount Service"]
e1 = bank_allocation_data["Amount Refrigeration"]

q1.index = q1.index.set_levels([["bank_expense",'allocation cost','sales', 'total cogs', 'direct costs'], ['total','cogs', 'subcontractors', 'transportation', 'warranties', 'recycling', 'staff cost', 'car cost', 'marketing cost', 'other direct costs'], ['total','salaries', 'subcontractors staff', 'clothing', 'Education', 'insurance', 'fuel', 'repair & service cost', 'leasing', 'insurance car', 'tax fees', 'parking & kteo & toll', 'advertising', 'hospitality', 'exhibition', 'subscriptions', 'other marketing cost', 'mobile', 'rent', 'other direct costs']])
w1.index = w1.index.set_levels([["bank_expense",'allocation cost','sales', 'total cogs', 'direct costs'], ['total','cogs', 'subcontractors', 'transportation', 'warranties', 'recycling', 'staff cost', 'car cost', 'marketing cost', 'other direct costs'], ['total','salaries', 'subcontractors staff', 'clothing', 'Education', 'insurance', 'fuel', 'repair & service cost', 'leasing', 'insurance car', 'tax fees', 'parking & kteo & toll', 'advertising', 'hospitality', 'exhibition', 'subscriptions', 'other marketing cost', 'mobile', 'rent', 'other direct costs']])
e1.index = e1.index.set_levels([["bank_expense",'allocation cost','sales', 'total cogs', 'direct costs'], ['total','cogs', 'subcontractors', 'transportation', 'warranties', 'recycling', 'staff cost', 'car cost', 'marketing cost', 'other direct costs'], ['total','salaries', 'subcontractors staff', 'clothing', 'Education', 'insurance', 'fuel', 'repair & service cost', 'leasing', 'insurance car', 'tax fees', 'parking & kteo & toll', 'advertising', 'hospitality', 'exhibition', 'subscriptions', 'other marketing cost', 'mobile', 'rent', 'other direct costs']])

bank_allocation_data

pnl_hvac_all_final = pd.concat([pnl_hvac_all.drop("bank_expense", level=0), q1])
pnl_service_all_final = pd.concat([pnl_service_all.drop("bank_expense", level=0), w1])
pnl_refrigeration_all_final = pd.concat([pnl_refrigeration_all.drop("bank_expense", level=0), e1])

pnl_hvac_all_final.rename(columns={1:'Jan', 2:'Feb', 3:'Mar', 4:"Apr",5:'May', 6:'Jun', 7:'Jul', 8:"Aug",9:'Sep', 10:'Oct', 11:'Nov', 12:"Dec"}, inplace=True)
pnl_service_all_final.rename(columns={1:'Jan', 2:'Feb', 3:'Mar', 4:"Apr",5:'May', 6:'Jun', 7:'Jul', 8:"Aug",9:'Sep', 10:'Oct', 11:'Nov', 12:"Dec"}, inplace=True)
pnl_refrigeration_all_final.rename(columns={1:'Jan', 2:'Feb', 3:'Mar', 4:"Apr",5:'May', 6:'Jun', 7:'Jul', 8:"Aug",9:'Sep', 10:'Oct', 11:'Nov', 12:"Dec"}, inplace=True)


# update cogs

def perbu_cogs(mycompletelist, pl_bu):
    service = []
    hvac = []
    refrigeration = []
    for each in mycompletelist:       
        bu = each[42]
        amount = each[43]
        date = each[23]
        count = each[20]
        fifo = each[10]
        cogs = count*fifo
        if  (bu == 20
            or   bu == 21
            or   bu == 22
            or   bu == 23
            or   bu == 24
            or   bu == 25
            or   bu == 26
            or   bu == 27
            or   bu == 28
            or   bu == 29
            or   bu == 30
            or   bu == 33
            or   bu == 61
            or   bu == 62
            or   bu == 63
            or   bu == 64
            or   bu == 65
            ):
                
                service.append([date, date.month, cogs])
                
        if  (bu == 40
            or   bu == 41
            or   bu == 60
            ):
                hvac.append([date, date.month, cogs])
                
        if  bu == 50:
                refrigeration.append([date, date.month, cogs])
               
    if pl_bu == "service":
        return service
    if pl_bu == "hvac":
        return hvac
    if pl_bu == "refrigeration":
        return refrigeration

    
service_cogs = perbu_cogs(alldata2, "service")
hvac_cogs = perbu_cogs(alldata2, "hvac")
refrigeration_cogs = perbu_cogs(alldata2, "refrigeration")

print(service_cogs)
# print(hvac_cogs)
# print(refrigeration_cogs)

def perbu_cogs_again(mycompletelist, pl_bu):
    service = []
    hvac = []
    refrigeration = []
    for each in mycompletelist:       
        bu = each[12]
        date = each[1]
        cogs = each[27]
        if  (bu == 20
            or   bu == 21
            or   bu == 22
            or   bu == 23
            or   bu == 24
            or   bu == 25
            or   bu == 26
            or   bu == 27
            or   bu == 28
            or   bu == 29
            or   bu == 30
            or   bu == 33
            or   bu == 61
            or   bu == 62
            or   bu == 63
            or   bu == 64
            or   bu == 65
            ):
                
                service.append([date, date.month, cogs])
                
        if  (bu == 40
            or   bu == 41
            or   bu == 60
            ):
                hvac.append([date, date.month, cogs])
                
        if  bu == 50:
                refrigeration.append([date, date.month, cogs])
               
    if pl_bu == "service":
        return service
    if pl_bu == "hvac":
        return hvac
    if pl_bu == "refrigeration":
        return refrigeration

    
service_cogs_again = perbu_cogs_again(alldata3, "service")
hvac_cogs_again = perbu_cogs_again(alldata3, "hvac")
refrigeration_cogs_again = perbu_cogs_again(alldata3, "refrigeration")

# print(service_cogs_again)
# print(hvac_cogs)
# print(refrigeration_cogs)


def renamewithmonths(dataframe):
    dataframe.rename(columns={1:'Jan', 2:'Feb', 3:'Mar', 4:"Apr",5:'May', 6:'Jun', 7:'Jul', 8:"Aug",9:'Sep', 10:'Oct', 11:'Nov', 12:"Dec"}, inplace=True)
    dataframe.columns.name = "Month"
    return dataframe

idx = pd.IndexSlice
df_service_cogs = pd.DataFrame.from_records(service_cogs)
df_hvac_cogs = pd.DataFrame.from_records(hvac_cogs)
df_refrigeration_cogs = pd.DataFrame.from_records(refrigeration_cogs)

cogssum1 = df_service_cogs.groupby([1], as_index=True).sum()
cogssum2 = df_hvac_cogs.groupby([1], as_index=True).sum()
cogssum3 = df_refrigeration_cogs.groupby([1], as_index=True).sum()

df_service_cogs_again = pd.DataFrame.from_records(service_cogs_again)
df_hvac_cogs_again = pd.DataFrame.from_records(hvac_cogs_again)
df_refrigeration_cogs_again = pd.DataFrame.from_records(refrigeration_cogs_again)

cogssum_again1 = df_service_cogs_again.groupby([1], as_index=True).sum()
cogssum_again2 = df_hvac_cogs_again.groupby([1], as_index=True).sum()
cogssum_again3 = df_refrigeration_cogs_again.groupby([1], as_index=True).sum()

# Service

cogssum_service = cogssum1.transpose()
cogssum_service2 = cogssum_again1.transpose() 
        
renamewithmonths(cogssum_service)
renamewithmonths(cogssum_service2)


service_r = cogssum_service.to_dict()
service_r2 = cogssum_service2.to_dict()
service_h = pnl_service_all_final.loc[idx[:,"cogs",:],:]
service_h2 = pnl_service_all_final.loc[idx["total cogs","total",:],:]
service_t = service_h.to_dict()
service_w2 = pnl_service_all_final.columns

def compare(dict1, dict2):
    for key in dict1.keys(): 
        if not key in dict2:
            dict2[str(key)] = 0
    return dict2

service_c1 = compare(service_t,service_r)
service_c2 = compare(service_t,service_r2)
        
service_w = pd.DataFrame.from_dict(service_c1) 
service_w3 = service_w2.tolist()
service_w = service_w[service_w3]

service_ww = pd.DataFrame.from_dict(service_c2) 
service_ww3 = service_w2.tolist()
service_ww = service_ww[service_ww3]

cogs_service = service_h.values + service_w.values + service_ww.values
total_cogs_service = service_h2.values + service_w.values + service_ww.values
pnl_service_all_final.loc[idx[:,"cogs",:]] = cogs_service
pnl_service_all_final.loc[idx["total cogs","total","total"]] = total_cogs_service


# HVAC

cogssum_hvac = cogssum2.transpose()
cogssum_hvac2 = cogssum_again2.transpose() 

renamewithmonths(cogssum_hvac)
renamewithmonths(cogssum_hvac2)

hvac_r = cogssum_hvac.to_dict()
hvac_r2 = cogssum_hvac2.to_dict()
hvac_h = pnl_hvac_all_final.loc[idx[:,"cogs",:],:]
hvac_h2 = pnl_hvac_all_final.loc[idx["total cogs","total",:],:]
hvac_t = hvac_h.to_dict()
hvac_w2 = pnl_hvac_all_final.columns

def compare(dict1, dict2):
    for key in dict1.keys(): 
        if not key in dict2:
            dict2[str(key)] = 0
    return dict2

hvac_c1 = compare(hvac_t,hvac_r)
hvac_c2 = compare(hvac_t,hvac_r2)
        
hvac_w = pd.DataFrame.from_dict(hvac_c1) 
hvac_w3 = hvac_w2.tolist()
hvac_w = hvac_w[hvac_w3]

hvac_ww = pd.DataFrame.from_dict(hvac_c2) 
hvac_ww3 = hvac_w2.tolist()
hvac_ww = hvac_ww[hvac_ww3]

cogs_hvac = hvac_h.values + hvac_w.values + hvac_ww.values
total_cogs_hvac = hvac_h2.values + hvac_w.values + hvac_ww.values
pnl_hvac_all_final.loc[idx[:,"cogs",:]] = cogs_hvac
pnl_hvac_all_final.loc[idx["total cogs","total","total"]] = total_cogs_hvac

# Refrigeration

cogssum_refrigeration = cogssum3.transpose()
cogssum_refrigeration2 = cogssum_again3.transpose()

renamewithmonths(cogssum_refrigeration)
renamewithmonths(cogssum_refrigeration2)

refrigeration_r = cogssum_refrigeration.to_dict()
refrigeration_r2 = cogssum_refrigeration2.to_dict()
refrigeration_h = pnl_refrigeration_all_final.loc[idx[:,"cogs",:],:]
refrigeration_h2 = pnl_refrigeration_all_final.loc[idx["total cogs","total",:],:]
refrigeration_t = refrigeration_h.to_dict()
refrigeration_w2 = pnl_refrigeration_all_final.columns

def compare(dict1, dict2):
    for key in dict1.keys(): 
        if not key in dict2:
            dict2[str(key)] = 0
    return dict2

refrigeration_c1 = compare(refrigeration_t,refrigeration_r)
refrigeration_c2 = compare(refrigeration_t,refrigeration_r2)
        
refrigeration_w = pd.DataFrame.from_dict(refrigeration_c1) 
refrigeration_w3 = refrigeration_w2.tolist()
refrigeration_w = refrigeration_w[refrigeration_w3]

refrigeration_ww = pd.DataFrame.from_dict(refrigeration_c2) 
refrigeration_ww3 = refrigeration_w2.tolist()
refrigeration_ww = refrigeration_ww[refrigeration_ww3]

cogs_refrigeration = refrigeration_h.values + refrigeration_w.values + refrigeration_ww.values
total_cogs_refrigeration = refrigeration_h2.values + refrigeration_w.values + refrigeration_ww.values
pnl_refrigeration_all_final.loc[idx[:,"cogs",:]] = cogs_refrigeration
pnl_refrigeration_all_final.loc[idx["total cogs","total","total"]] = total_cogs_refrigeration



# add statistics and calculations
### add statistics and calculations

#gross profit
def grossprofit(pnl_all_final):
    gross_profit = pnl_all_final.loc[idx["sales"]].values - pnl_all_final.loc[idx["total cogs","total","total"]].values

    gross_profit_df = pd.DataFrame.from_records(gross_profit)
    gross_profit_df.columns = pnl_all_final.columns
    gross_profit_df["Category A"] = "gross_profit"
    gross_profit_df["Category B"] = "total"
    gross_profit_df["Category C"] = "total"

    a4 = gross_profit_df["Category A"].astype('category')
    b4 = gross_profit_df["Category B"].astype('category')
    c4 = gross_profit_df["Category C"].astype('category')

    indexa4 = pd.CategoricalIndex(a4, categories=["sales","total cogs","direct costs","allocation cost","bank_expense","gross_profit","ebitda","ebtda"])
    indexb4 = pd.CategoricalIndex(b4, categories=["cogs","subcontractors","transportation","warranties","recycling","staff cost","car cost","marketing cost","other direct costs","total"])
    indexc4 = pd.CategoricalIndex(c4, categories=["salaries","subcontractors staff","clothing","Education","insurance","fuel","repair & service cost","leasing","insurance car", "tax fees","parking & kteo & toll", "advertising","hospitality","exhibition","subscriptions","other marketing cost","mobile","rent","other direct costs","total"])

    gross_profit_new_index = gross_profit_df.set_index([indexa4, indexb4, indexc4])

    gross_profit_final = gross_profit_new_index.drop(columns=["Category A", "Category B","Category C"])

    pnl_update = pd.concat([pnl_all_final, gross_profit_final])

    return pnl_update
    
pnl_refrigeration_gross = grossprofit(pnl_refrigeration_all_final)
pnl_service_gross = grossprofit(pnl_service_all_final)
pnl_hvac_gross = grossprofit(pnl_hvac_all_final)
pnl_service_gross

#EBITDA

def ebitda(pnl_all_final):
    ebitda = pnl_all_final.loc[idx["sales"]].values - pnl_all_final.loc[idx["total cogs","total","total"]].values - pnl_all_final.loc[idx["direct costs","total","total"]].values -  pnl_all_final.loc[idx["allocation cost","total","total"]].values

    ebitda_df = pd.DataFrame.from_records(ebitda)
    ebitda_df.columns = pnl_all_final.columns
    ebitda_df["Category A"] = "ebitda"
    ebitda_df["Category B"] = "total"
    ebitda_df["Category C"] = "total"

    a5 = ebitda_df["Category A"].astype('category')
    b5 = ebitda_df["Category B"].astype('category')
    c5 = ebitda_df["Category C"].astype('category')

    indexa5 = pd.CategoricalIndex(a5, categories=["sales","total cogs","direct costs","allocation cost","bank_expense","gross_profit","ebitda","ebtda"])
    indexb5 = pd.CategoricalIndex(b5, categories=["cogs","subcontractors","transportation","warranties","recycling","staff cost","car cost","marketing cost","other direct costs","total"])
    indexc5 = pd.CategoricalIndex(c5, categories=["salaries","subcontractors staff","clothing","Education","insurance","fuel","repair & service cost","leasing","insurance car", "tax fees","parking & kteo & toll", "advertising","hospitality","exhibition","subscriptions","other marketing cost","mobile","rent","other direct costs","total"])

    ebitda_new_index = ebitda_df.set_index([indexa5, indexb5, indexc5])

    ebitda_final = ebitda_new_index.drop(columns=["Category A", "Category B","Category C"])

    pnl_update_ebitda = pd.concat([pnl_all_final, ebitda_final])

    return pnl_update_ebitda

pnl_refrigeration_ebitda = ebitda(pnl_refrigeration_gross)
pnl_service_ebitda = ebitda(pnl_service_gross)
pnl_hvac_ebitda = ebitda(pnl_hvac_gross)

pnl_service_ebitda

#EBTDA

def ebtda(pnl_all_final):
    ebtda = pnl_all_final.loc[idx["sales"]].values - pnl_all_final.loc[idx["total cogs","total","total"]].values - pnl_all_final.loc[idx["direct costs","total","total"]].values - pnl_all_final.loc[idx["allocation cost","total","total"]].values  - pnl_all_final.loc[idx["bank_expense","total","total"]].values

    ebtda_df = pd.DataFrame.from_records(ebtda)
    ebtda_df.columns = pnl_all_final.columns
    ebtda_df["Category A"] = "ebtda"
    ebtda_df["Category B"] = "total"
    ebtda_df["Category C"] = "total"

    a6 = ebtda_df["Category A"].astype('category')
    b6 = ebtda_df["Category B"].astype('category')
    c6 = ebtda_df["Category C"].astype('category')

    indexa6 = pd.CategoricalIndex(a6, categories=["sales","total cogs","direct costs","allocation cost","bank_expense","gross_profit","ebitda","ebtda"])
    indexb6 = pd.CategoricalIndex(b6, categories=["cogs","subcontractors","transportation","warranties","recycling","staff cost","car cost","marketing cost","other direct costs","total"])
    indexc6 = pd.CategoricalIndex(c6, categories=["salaries","subcontractors staff","clothing","Education","insurance","fuel","repair & service cost","leasing","insurance car", "tax fees","parking & kteo & toll", "advertising","hospitality","exhibition","subscriptions","other marketing cost","mobile","rent","other direct costs","total"])

    ebtda_new_index = ebtda_df.set_index([indexa6, indexb6, indexc6])

    ebtda_final = ebtda_new_index.drop(columns=["Category A", "Category B","Category C"])

    pnl_update_ebtda = pd.concat([pnl_all_final, ebtda_final])

    return pnl_update_ebtda

pnl_refrigeration_ebtda = ebtda(pnl_refrigeration_ebitda)
pnl_service_ebtda = ebtda(pnl_service_ebitda)
pnl_hvac_ebtda = ebtda(pnl_hvac_ebitda)

# create total p&l

pnl_total_all_final = pnl_service_ebtda + pnl_refrigeration_ebtda + pnl_hvac_ebtda


#prepare data for dash tables

def reset_index(df):
    index_df = df.index.to_frame(index=False)
    df = df.reset_index(drop=True)
    #  In merge is important the order in which you pass the dataframes
    # if the index contains a Categorical. 
    # pd.merge(df, index_df, left_index=True, right_index=True) does not work
    return pd.merge(index_df, df, left_index=True, right_index=True)
    
reset_hvac = reset_index(pnl_hvac_ebtda)
reset_refrigeration = reset_index(pnl_refrigeration_ebtda)
reset_service = reset_index(pnl_service_ebtda)
reset_total = reset_index(pnl_total_all_final)

d_hvac = reset_hvac.to_dict("rows")
d_refrigeration= reset_refrigeration.to_dict("rows")
d_service = reset_service.to_dict("rows")
d_total = reset_total.to_dict("rows")

def dash_table(d_table):
    for each in d_table:
        for key, value in each.items():  
            if isinstance(value, float):
                newvalue = ('{:,.2f}'.format(round(value, 2))
                            .replace(',', ' ')  # 'save' the thousands separators
                            .replace('.', ',')  # dot to comma
                            .replace(' ', '.'))
                each[key] = newvalue
        
    return d_table
  

dash_hvac = dash_table(d_hvac)    
dash_refrigeration = dash_table(d_refrigeration) 
dash_service = dash_table(d_service) 
dash_total = dash_table(d_total)

order = [0,6,1,2,3,4,5,33,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,34,35]
dash_ref = [dash_refrigeration[i] for i in order]
dash_ser = [dash_service[i] for i in order]
dash_air = [dash_hvac[i] for i in order]
dash_tot = [dash_total[i] for i in order]



external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


datatableHVAC = dt.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in reset_hvac.columns],
    data= dash_air,
    style_table={'overflowX': 'scroll'},
    style_header={
        'backgroundColor': 'white',
        'fontWeight': 'bold',
        'color': 'black',
        'font-family': "arial",
    },
    style_cell={
        'textAlign': 'left',
        'font-family': "arial"
    },
    style_cell_conditional=[
        {
            'if': {'column_id': 'Jan'},
            'width': '15%',

        },
           {
            'if': {'column_id': 'Feb'},
            'width': '15%',

        },
        { 'if': {'column_id': 'Mar'},
            'width': '15%',

        },
            {
        "if": {"row_index": 0},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                {
        "if": {"row_index": 1},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                {
        "if": {"row_index": 7},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                    {
        "if": {"row_index": 31},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                    {
        "if": {"row_index": 32},
        "backgroundColor": "#3D9970",
        'color': 'white'
    }
    ],
    css=[{
        'selector': '.dash-cell div.dash-cell-value',
        'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;',

    }])

datatableRefrigeration = dt.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in reset_refrigeration.columns],
    data= dash_ref,
    style_table={'overflowX': 'scroll'},
    style_header={
        'backgroundColor': 'white',
        'fontWeight': 'bold',
        'color': 'black',
        'font-family': "arial",
    },
    style_cell={
        'textAlign': 'left',
        'font-family': "arial"
    },
    style_cell_conditional=[
        {
            'if': {'column_id': 'Jan'},
            'width': '15%',

        },
           {
            'if': {'column_id': 'Feb'},
            'width': '15%',

        },
        { 'if': {'column_id': 'Mar'},
            'width': '15%',

        },
        {
            'if': {'column_id': 'amount 1'},
            'textAlign': 'left'
        },
            {
        "if": {"row_index": 0},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                {
        "if": {"row_index": 1},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                {
        "if": {"row_index": 7},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                    {
        "if": {"row_index": 31},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                    {
        "if": {"row_index": 32},
        "backgroundColor": "#3D9970",
        'color': 'white'
    }
    ],
    css=[{
        'selector': '.dash-cell div.dash-cell-value',
        'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;',

    }])


datatableService = dt.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in reset_service.columns],
    data= dash_ser,
    style_table={'overflowX': 'scroll'},
    style_header={
        'backgroundColor': 'white',
        'fontWeight': 'bold',
        'color': 'black',
        'font-family': "arial",
    },
    style_cell={
        'textAlign': 'left',
        'font-family': "arial"
    },
    style_cell_conditional=[
        {
            'if': {'column_id': 'Jan'},
            'width': '15%',

        },
           {
            'if': {'column_id': 'Feb'},
            'width': '15%',

        },
        { 'if': {'column_id': 'Mar'},
            'width': '15%',

        },
        {
            'if': {'column_id': 'amount 1'},
            'textAlign': 'left'
        },
            {
        "if": {"row_index": 0},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                {
        "if": {"row_index": 1},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                {
        "if": {"row_index": 7},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                    {
        "if": {"row_index": 31},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                    {
        "if": {"row_index": 32},
        "backgroundColor": "#3D9970",
        'color': 'white'
    }
    ],
    css=[{
        'selector': '.dash-cell div.dash-cell-value',
        'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;',

    }])

datatableTotal = dt.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in reset_total.columns],
    data= dash_tot,
    style_table={'overflowX': 'scroll'},
    style_header={
        'backgroundColor': 'white',
        'fontWeight': 'bold',
        'color': 'black',
        'font-family': "arial",
    },
    style_cell={
        'textAlign': 'left',
        'font-family': "arial"
    },
    style_cell_conditional=[
        {
            'if': {'column_id': 'Jan'},
            'width': '15%',

        },
           {
            'if': {'column_id': 'Feb'},
            'width': '15%',

        },
        { 'if': {'column_id': 'Mar'},
            'width': '15%',

        },
        {
            'if': {'column_id': 'amount 1'},
            'textAlign': 'left'
        },
            {
        "if": {"row_index": 0},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                {
        "if": {"row_index": 1},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                {
        "if": {"row_index": 7},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                    {
        "if": {"row_index": 31},
        "backgroundColor": "#3D9970",
        'color': 'white'
    },
                    {
        "if": {"row_index": 32},
        "backgroundColor": "#3D9970",
        'color': 'white'
    }
    ],
    css=[{
        'selector': '.dash-cell div.dash-cell-value',
        'rule': 'display: inline; white-space: inherit; overflow: inherit; text-overflow: inherit;',

    }])

all_options = {
    'P&L': ['Total', 'HVAC', 'Refrigeration', 'Service'],
    'Sales Metrics': ["Test"],
    'Operational Metrics': ["Test"],
    'Other Financial Metrics': ["Test"]
}

     
app.layout = html.Div(children=[
    html.H4(children='DELPHIS EXECUTIVE DASHBOARD'),
    html.Div(children=[
        html.Div(children =[
            html.Label('Please Select Application'),
            dcc.Dropdown(
                id='application-dropdown',
                options=[{'label': k, 'value': k} for k in all_options.keys()],
                value='P&L',
                style={"width": 200}
            )],style = {'display': 'inline-block'}),
        html.Div(children =[
            html.Label('Please Select Category'),
            dcc.Dropdown(
                id='category-dropdown',
                style={"width": 200}
            )],style = {'display': 'inline-block','margin-left': 20 })
        ]),
    html.Div(id='display-selected-values', style={'width': 650, 'margin-top': 40, 'margin-right':20})
    
], style={'padding': 2, 'margin-left': 20})


@app.callback(
    Output('category-dropdown', 'options'),
    [Input('application-dropdown', 'value')])
def set_category_options(selected_application):
    return [{'label': i, 'value': i} for i in all_options[selected_application]]

@app.callback(
    Output('category-dropdown', 'value'),
    [Input('category-dropdown', 'options')])
def set_category_value(available_options):
    return available_options[0]['value']

@app.callback(
    Output('display-selected-values', 'children'),
    [Input('application-dropdown', 'value'),
     Input('category-dropdown', 'value')])
def set_display_children(selected_application, selected_category):
    if selected_category == "Total":
        return datatableTotal,
    if selected_category == "HVAC":
        return datatableHVAC,
    if selected_category == "Refrigeration":
        return datatableRefrigeration,
    if selected_category == "Service":
        return datatableService,


if __name__ == '__main__':
    app.run_server(debug=True)